﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class TeamSetupMessage : MessageBase {

	public int team;
	public int playerClass;
}
