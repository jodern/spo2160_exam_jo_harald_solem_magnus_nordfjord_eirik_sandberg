﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class GameController : NetworkBehaviour {

	public static GameController instance;

	public Team[] teams;
	public const int maxHoldTime = 20;
	public static int teamToPick;

    [SerializeField] GameObject redPickbutton, bluePickButton;
    public CaptureFlag kothPoint;


	void Awake()
	{ 
		if (instance == null)
			instance = this;
		else
			Destroy (this);

        kothPoint = FindObjectOfType<CaptureFlag>();
	}


	void Start()
	{
		
	}


    //Eneable team selection when player joins game
    public override void OnStartClient()
    {
        base.OnStartClient();
        redPickbutton.SetActive(true);
        bluePickButton.SetActive(true);
    }


    //Add the player and set the team to pick for when the player spawns
    [Client] public void SpawnPlayer(int team)
	{
		teamToPick = team;
		redPickbutton.SetActive(false);
		bluePickButton.SetActive(false);
		TeamSetupMessage msg = new TeamSetupMessage();
		msg.team = team;
		msg.playerClass = 0;
		ClientScene.AddPlayer(connectionToServer, 0, msg);
	}


    //Team captures a point to allow for timer ro diminish
	public void CapturePoint(int teamIndex)
	{
		for (int i = 0; i < teams.Length; i++)
		{
			if (i == teamIndex)
			{
				teams[i].hasPoint = true;
			}
			else
			{
				teams[i].hasPoint = false;
			}
		}
	}


    //A team has won!
	public void Win(int team)
	{
		RpcWin(team);
		Time.timeScale = 0;
	}


    //Call on client to let them know a TEAM HAS WON!
	[ClientRpc] void RpcWin(int team)
	{
		Time.timeScale = 0;
		Debug.Log(teams[team].name + " WINS!");
	}
}
