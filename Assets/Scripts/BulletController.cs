﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BulletController : NetworkBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnCollisionEnter(Collision col)
	{
        PlayerController player = col.gameObject.GetComponent<PlayerController>();
        if (isServer)
		{
			
			if (player != null) 
			{
				player.TakeDamage (20);
                Debug.Log("Hit " + player.team.name + " player!");
			}
		}
        //Do not hit local player
        if(!player.isLocalPlayer)
		    Destroy (gameObject);
	}
}
