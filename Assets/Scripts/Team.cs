﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Team : NetworkBehaviour {

	public int layer;
	public Color teamColor;
	public string name;
	public bool hasPoint;
	[SyncVar(hook = "OnHoldPointTimer")] public float holdPointTimer;

	[SerializeField] Text timer;

	public int teamIndex;

	void Start()
	{
		holdPointTimer = GameController.maxHoldTime;
		teamIndex = System.Array.IndexOf(GameController.instance.teams, this);
        timer.text = holdPointTimer.ToString("0");
	}


	void Update()
	{
		if (!isServer)
			return;
		
		if (hasPoint) 
		{
			holdPointTimer -= Time.deltaTime;
            bool fullyCapped = (name == "Red") ? (GameController.instance.kothPoint.capProgress == -1) : (GameController.instance.kothPoint.capProgress == 1);
            if (holdPointTimer <= 0 && fullyCapped && Time.timeScale > 0) 
			{
				holdPointTimer = 0;
				GameController.instance.Win(teamIndex);
			}
		}
	}


	void OnHoldPointTimer(float value)
	{
		timer.text = value.ToString ("0");
	}
}
