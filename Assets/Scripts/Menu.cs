﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Menu : MonoBehaviour {

	// hooks up the UI elements into the script
	[SerializeField] Text gameNameInputText;
	[SerializeField] GameObject findGameInputField;
	[SerializeField] Button createGameButton, joinGameButton;

	public ExamNetworkManager network;

	// Closes the game when Exit button is pressed	
	public void OnClickApplicationQuit()
	{
		Application.Quit ();
	}

	// Activates the text field for ip you want to connect to and the Join game button
	public void OnClickFindGameInputField()
		{
			findGameInputField.SetActive (!findGameInputField.activeInHierarchy);
			joinGameButton.gameObject.SetActive (!joinGameButton.gameObject.activeInHierarchy);
		}

	// Creates a local game through the Network manager
	public void OnClickcreateGameButton()
	{
		network.StartHost();
	}

	// Joins a game with the ip you typed in the text field
	public void OnClickjoinGameButton()
	{
		network.StartClient ();
	}


//	Started working on a server manager but then figured we were to use the unity one

//	public bool isAtStartup = true;
//	NetworkClient myClient;

//	public void SetupServer()
//	{
//		NetworkServer.Listen (7777);
//		isAtStartup = false;
//	}
//
//	public void SetupClient ()
//	{
//		myClient = new NetworkClient();
//		myClient.RegisterHandler(MsgType.Connect, OnConnected);
//		myClient.Connect(gameNameInputText.text, 7777);
//		isAtStartup = false;
//	}
//
//	public void SetupLocalClient()
//	{
//		myClient = ClientScene.ConnectLocalServer ();
//		myClient.RegisterHandler(MsgType.Connect, OnConnected);
//		isAtStartup = false;
//	}
//	public void OnConnected(NetworkMessage netMsg)
//	{
//		Debug.Log ("Connected to server");
//	}
}