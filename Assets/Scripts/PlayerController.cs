﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour {

	CharacterController ctrl;
	Vector2 mouseDelta = Vector2.zero;
	Vector3 deltaMove = Vector3.zero;
	[SerializeField] float groundSpeed = 1;
	[SerializeField] float airSpeed = 1;
    [SerializeField] float jumpSpeed = 10;
	[SerializeField] Image healthBar;
	[SerializeField] int health = 100;
	public Team team;

	[SerializeField] GameObject bullet;
    Transform bulletSpawn;
    [SerializeField]float minFireDelay = 0.5f;
    float fireTimestamp = 0;
    Camera camera;
    bool isDead = false;

	[SerializeField] AudioClip shootSound;
	[SerializeField] AudioClip deathSound;

    AudioSource audioS;

	// Use this for initialization
	void Awake () 
	{
        audioS = GetComponent<AudioSource>();
		ctrl = GetComponent<CharacterController> ();
        bulletSpawn = transform.GetChild(0);
	}

    //Make player update its team on server and display team on client
	[Command] void CmdSetTeam(int t)
	{
        foreach (PlayerController player in FindObjectsOfType<PlayerController>())
        {
            if (player == this)
                player.RpcSetTeam(t);
            else
                player.RpcSetTeam(player.team.teamIndex);
        }
        team = GameController.instance.teams[t];
        gameObject.layer = team.layer;
    }


	[ClientRpc] void RpcSetTeam(int t)
	{
		team = GameController.instance.teams[t];
		Debug.Log("Added player on " + team.name);
		GetComponentInChildren<Renderer>().material.color = team.teamColor;
		gameObject.layer = team.layer;
	}



	
	// Update is called once per frame
	void Update () 
	{
		//All other clients healthbars should look at the local player
		if (isClient && !isLocalPlayer)
        {
            if(healthBar && Camera.main)
				healthBar.transform.parent.LookAt(Camera.main.transform.position);
        }

        if (!isLocalPlayer || isDead)
			return;

		mouseDelta += new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); //Get the mouse input in movement off start rot


		//Clamps view from roughly straight down to straight up
		mouseDelta.y = ClampAngle(mouseDelta.y, -88, 88);

		//Transform movement into horizontal rotation
		Quaternion xQuat = Quaternion.AngleAxis(mouseDelta.x, Vector3.up);
		//rotation around left axis
		Quaternion yQuat = Quaternion.AngleAxis(mouseDelta.y, Vector3.left);

		Camera.main.transform.localRotation = yQuat; //Set camera local rotation to vertical rot off start 
		transform.localRotation = xQuat;

        Vector3 keyInput = transform.TransformDirection(new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"))).normalized;

		if (ctrl.isGrounded) 
		{
			deltaMove = keyInput * groundSpeed * Time.deltaTime;
		} 
		else 
		{
			deltaMove += keyInput * airSpeed * Time.deltaTime;
		}

        //Jump
        if (ctrl.isGrounded) 
        {
            if(Input.GetKeyDown(KeyCode.Space))
                deltaMove.y += jumpSpeed;
        }

		deltaMove += Physics.gravity * Time.deltaTime;
		ctrl.Move (deltaMove);


        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time > fireTimestamp + minFireDelay)
            {
                fireTimestamp = Time.time;

                audioS.PlayOneShot(shootSound);

                //Aim for whatever is in the center of the screen
                Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width * 0.5f, Screen.height * 0.5f));
				RaycastHit hit;
				int layer = 1 << team.layer;
				layer = ~layer;
				if (Physics.Raycast(ray, out hit, layer))
				{
					if (hit.collider)
					{
						Debug.DrawLine (bulletSpawn.position, hit.point, Color.red, 3);
						CmdFire ((hit.point - bulletSpawn.position).normalized);
					}
					else 
					{
						Debug.DrawRay(bulletSpawn.position, camera.transform.forward * 10, Color.red, 3);
						CmdFire(camera.transform.forward);
					}
				}
				else
				{
					Debug.DrawRay(bulletSpawn.position, camera.transform.forward * 10, Color.red, 3);
					CmdFire(camera.transform.forward);
				}
            }
   
        }

        //cursor lock, unlock
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        if(Input.GetMouseButtonDown(0))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
	}


	public void TakeDamage(int damage)
    {
        health -= damage;
        RpcUpdateHealthBar(health);

        if (health <= 0)
        {
            health = 100;
            StartCoroutine(WaitRespawn());
			RpcSetPlyActive(false);
			RpcUpdateHealthBar(health);
            audioS.clip = deathSound;
            audioS.Play();
			RpcPlaySound (deathSound);
        }
    }


	[ClientRpc] void RpcPlaySound(AudioClip clip)
	{
        audioS.clip = clip;
        audioS.Play();
	}
        

    [ClientRpc] void RpcSetPlyActive(bool state)
    {
        isDead = !state;
        foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            rend.enabled = state;
        GetComponent<Collider>().enabled = state;
        if (isLocalPlayer)
            camera.enabled = state;
        if(!isLocalPlayer)
            healthBar.gameObject.SetActive(state);
    }


    IEnumerator WaitRespawn()
    {
        print("Wait respawn");
        RpcSetPlyActive(false);
        yield return new WaitForSeconds(5);
        print("Wait over");
        RpcSetPlyActive(true);
        RpcRespawn(); 
    }


    [ClientRpc] void RpcUpdateHealthBar(int health)
    {
		healthBar.fillAmount = (float)(health / 100f);
    }


    [ClientRpc] void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            NetworkManager manager = FindObjectOfType<ExamNetworkManager>();
            transform.position = manager.GetStartPosition().position;
        }
    }


	[Command] void CmdFire(Vector3 dir)
    {
        RpcPlaySound (shootSound);
        GameObject instbullet = Instantiate(bullet, bulletSpawn.position, Quaternion.LookRotation(dir) * Quaternion.Euler(90,0,0)) as GameObject;
		instbullet.layer = team.layer;

        instbullet.GetComponent<Rigidbody>().velocity = dir * 100;
        NetworkServer.Spawn(instbullet);
        Destroy(instbullet, 3);
    }


	//Clamp an angle between two values
	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;
		return Mathf.Clamp (angle, min, max);
	}


	public override void OnStartLocalPlayer()
	{
		print ("start");
		Camera.main.transform.parent = transform.FindChild("CameraSlot");
		Camera.main.transform.localPosition = Vector3.zero;
        camera = Camera.main;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
        //Healthbar should not be visible above the local player
        healthBar.gameObject.SetActive(false);
		healthBar = GameObject.Find ("ClientHealthBar").GetComponent<Image> ();

		GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
		CmdSetTeam(GameController.teamToPick);

	}
}
