﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ExamNetworkManager : NetworkManager {


	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
	{
		GameObject player = Instantiate(playerPrefab, GetStartPosition().position, Quaternion.identity) as GameObject;
		int t = extraMessageReader.ReadMessage<TeamSetupMessage>().team;
		Debug.Log(t);
		PlayerController ctrl = player.GetComponent<PlayerController>();
		ctrl.team = GameController.instance.teams[t];
		Debug.LogError("Added player on " + ctrl.team.name);
		player.GetComponentInChildren<Renderer>().material.color = ctrl.team.teamColor;
		player.layer = ctrl.team.layer;
		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
	}
}
