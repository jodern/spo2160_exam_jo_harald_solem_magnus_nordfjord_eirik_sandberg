﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using System.Collections.Generic;

public class CaptureFlag : NetworkBehaviour {

	private Team currentTeam = null;
	public	List<Team> Intrigger = new List<Team>();
	public Material flagcollor;
    public Slider capProgressSlider;
    [SyncVar(hook = "OnCapProgress")]public float capProgress = 0;


	void OnTriggerEnter(Collider Player)
	{
		PlayerController Check = Player.GetComponent<PlayerController>(); 
		Debug.Log ("triggerd");
		if(Check != null)
		{
			Intrigger.Add (Check.team);
			if (Check.team != currentTeam) 
			{
			//currentTeam = Check.team;
			flagcollor.color = currentTeam.teamColor;
			Debug.Log (currentTeam.name);
			}
		}
	}


    void OnCapProgress(float value)
    {
        capProgressSlider.value = value;
    }


    void FixedUpdate()
    {
        if (!isServer)
            return;

        if (Intrigger.Count > 0)
        {
            int redCount = Intrigger.Count(i => i.name == "Red");
            int blueCount = Intrigger.Count - redCount;
            if (redCount > blueCount)
            {
                if (currentTeam == null || currentTeam.name != "Red")
                {
                    capProgress = Mathf.MoveTowards(capProgress, -1, Time.fixedDeltaTime * 0.1f);
                    if (capProgress <= -1)
                    {
                        capProgress = -1;
                        GameController.instance.CapturePoint(0);
                        currentTeam = GameController.instance.teams[0];
                    }
                }
                return;
            }
            else if (blueCount > redCount)
            {
                if (currentTeam == null || currentTeam.name != "Blue")
                {
                    capProgress = Mathf.MoveTowards(capProgress, 1, Time.fixedDeltaTime * 0.1f);
                    if (capProgress >= 1)
                    {
                        capProgress = 1;
                        GameController.instance.CapturePoint(1);
                        currentTeam = GameController.instance.teams[1];
                    }
                }
                return;
            }
        }
        if (currentTeam == null)
            capProgress = Mathf.MoveTowards(capProgress, 0, Time.fixedDeltaTime * 0.2f);
        else if (currentTeam.name == "Red")
            capProgress = Mathf.MoveTowards(capProgress, -1, Time.fixedDeltaTime * 0.2f);
        else
            capProgress = Mathf.MoveTowards(capProgress, 1, Time.fixedDeltaTime * 0.2f);
    }


    void OnTriggerExit(Collider Player)
	{
		PlayerController Check = Player.GetComponent<PlayerController>();
		Debug.Log ("Untriggerd");
		if (Check != null) 
		{
			Intrigger.Remove (Check.team);
		}
	}
}
