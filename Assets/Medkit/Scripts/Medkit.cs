﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Medkit : NetworkBehaviour {

	float timer = 0f;
	bool isActive = true;

	void OnTriggerEnter (Collider col)
	{

		if (col.tag == "Player")
		{

			//PlayerController player = 
			Debug.Log ("I gave you life!");

			timer = 0f;

			isActive = false;
			GetComponent <MeshRenderer>().enabled = false;
			GetComponent <BoxCollider>().enabled = false;	
		}

	}

	void Update (){

		if (!isActive);{

		timer += Time.deltaTime;

		if (timer >= 30) {
			isActive = true;
			GetComponent <MeshRenderer> ().enabled = true;
			GetComponent <BoxCollider> ().enabled = true;
		}


		}
	}

}


// A script for the medkit which will give the players health points. -	Eirik J. //